############################################################################
# -*- coding: utf-8 -*-                                                    #
#   Copyright © 2016 José Manuel Santamaría Lema <panfaust@gmail.com>      #
#                                                                          #
#   This program is free software; you can redistribute it and/or modify   #
#   it under the terms of the GNU General Public License as published by   #
#   the Free Software Foundation; either version 2 of the License, or      #
#   (at your option) any later version.                                    #
############################################################################

from __future__ import print_function

import sys
import re

#Status consts
STATUS_SUCCESS = 0
STATUS_WARNING = 1
STATUS_ERROR = 2
STATUS_BUILDING = -1
STATUS_WAITING = -2

def get_autopkgtest(log,testname,package):
    start = log.index("test %s: [-" % testname)
    end = log.rindex("test %s: -----------------------]" % testname)
    more_lines = 0
    while more_lines < 3:
        if log[end] == '\n':
            more_lines += 1
            if more_lines == 2:
                last_line_start = end+1
            elif more_lines == 3:
                last_line_end = end
        end += 1
    last_line = log[last_line_start:last_line_end]
    text = log[start:end]
#    #Debugging
#    if package == "kdelibs4support":
#        print('start: %s, end %s' % (start,end), file=sys.stderr)
#        print('last line: %s' % last_line, file=sys.stderr)
#        print('text: %s' % text, file=sys.stderr)
    if re.match('%s.*PASS'%testname, last_line) != None:
        status = STATUS_SUCCESS
    else:
        status = STATUS_ERROR
    return (text,status)

# kate: space-indent on; indent-width 4; replace-tabs on; indent-mode python; remove-trailing-space modified;
# vim: expandtab ts=4
