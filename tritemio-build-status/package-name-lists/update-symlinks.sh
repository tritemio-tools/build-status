#!/bin/sh

TRITEMIO_SUITES="ubuntu-exp ubuntu-exp2 ubuntu-exp3"
RELEASE_TYPES="qt frameworks plasma applications"
UBUNTU_UNSTABLE_NAME="zesty"

for ts in $TRITEMIO_SUITES; do
	for rt in $RELEASE_TYPES; do
		ln -sf $rt-zesty $rt-$ts 
	done
done
